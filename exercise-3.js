/**
 * #############################
 * ##  E J E R C I C I O   3  ##
 * #############################
 *
 * Utiliza los métodos map, filter o reduce para resolver las siguientes propuestas:
 *
 *  - 1. Obtén la suma total de todas las edades de las personas.
 *  - 2. Obtén la suma total de todas las edades de las personas francesas.
 *  - 3. Obtén un array con el nombre de todas las mascotas.
 *  - 4. Obtén un array con las personas que tengan gato.
 *  - 5. Obtén un array con los coches de los españoles.
 *  - 6. Obtén un array con las personas que tengan un coche de la marca Ford.
 *  - 7. ¡Bonus point! Obtén un array con todas las personas en el que cada persona tenga toda
 *       la info de su coche. Ejemplo a continuación:
 *
 *      [
 *          {
 *               name: 'Berto',
 *               country: 'ES',
 *               age: 44,
 *               car: {
 *                  id: 'LU9286V',
 *                  brand: 'Citroen',
 *                  model: 'Xsara'
 *               },
 *               pet: {
 *                   name: 'Moon',
 *                   type: 'perro'
 *               }
 *           },
 *           (...)
 *      ]
 *
 *  Tip: en algún caso es probable que el método "nombreArray.find()" te sea de ayuda.
 *
 */

'use strict';

const persons = [
  {
    name: 'Berto',
    country: 'ES',
    age: 44,
    car: 'LU9286V',
    pet: {
      name: 'Moon',
      type: 'perro',
    },
  },
  {
    name: 'Jess',
    country: 'UK',
    age: 29,
    car: 'GB2913U',
    pet: {
      name: 'Kit',
      type: 'gato',
    },
  },
  {
    name: 'Tom',
    country: 'UK',
    age: 36,
    car: 'GB8722N',
    pet: {
      name: 'Rex',
      type: 'perro',
    },
  },
  {
    name: 'Alexandre',
    country: 'FR',
    age: 19,
    car: 'FT5386P',
    pet: {
      name: 'Aron',
      type: 'gato',
    },
  },
  {
    name: 'Rebeca',
    country: 'ES',
    age: 32,
    car: 'MD4578T',
    pet: {
      name: 'Carbón',
      type: 'gato',
    },
  },
  {
    name: 'Stefano',
    country: 'IT',
    age: 52,
    car: 'LP6572I',
    pet: {
      name: 'Bimbo',
      type: 'perro',
    },
  },
  {
    name: 'Colette',
    country: 'FR',
    age: 22,
    car: 'FU8929P',
    pet: {
      name: 'Amadeu',
      type: 'gato',
    },
  },
];

const cars = [
  {
    id: 'LU9286V',
    brand: 'Citroen',
    model: 'Xsara',
  },
  {
    id: 'GB2913U',
    brand: 'Fiat',
    model: 'Punto',
  },
  {
    id: 'GB8722N',
    brand: 'Opel',
    model: 'Astra',
  },
  {
    id: 'FT5386P',
    brand: 'Ford',
    model: 'Focus',
  },
  {
    id: 'MD4578T',
    brand: 'Opel',
    model: 'Corsa',
  },
  {
    id: 'LP6572I',
    brand: 'Ford',
    model: 'Fiesta',
  },
  {
    id: 'FU8929P',
    brand: 'Fiat',
    model: 'Uno',
  },
];

//ejercicio 1:
console.log('Ejercicio 1:');
const sumaEdades = persons.reduce((acumulador, edades) => (acumulador += edades.age), 0);
console.log(sumaEdades);

//ejercicio 2:
console.log('Ejercicio 2:');
const sumaEdadesFranceses = persons.reduce(
  (acumulador, edadesFranceses) =>
    edadesFranceses.country === 'FR' ? (acumulador += edadesFranceses.age) : acumulador,
  0
);
console.log(sumaEdadesFranceses);

//ejercicio 3:
console.log('Ejercicio 3:');
const nombresMascotas = persons.map((mascota) => mascota.pet.name);
console.log(nombresMascotas);

//ejercicio 4:
console.log('Ejercicio 4:');
const personasConGato = persons.filter((persona) => persona.pet.type === 'gato');
console.log(personasConGato);

//ejercicio 5:
console.log('Ejercicio 5:');
const personsCars = persons.map((person) => {
  const stringifyPerson = JSON.stringify(person);
  const copyPerson = JSON.parse(stringifyPerson); //hago la copia profunda
  const personsCars = cars.find((car) => copyPerson.car === car.id);
  copyPerson.car = personsCars;
  return copyPerson;
}); //unifico los dos arrays...
console.log(personsCars);

const cocheEspañoles = personsCars.reduce((acumulador, person) => {
  if (person.country === 'ES') {
    acumulador.push(person.car);
  }
  return acumulador;
}, []);

console.log(cocheEspañoles);

//ejercicio 6:
console.log('Ejercicio 6:');
const personasConCochesFord = personsCars.filter((marca) => marca.car.brand === 'Ford');
console.log(personasConCochesFord);

//ejercicio 7:
console.log('Ejercicio 7:');
//resuelto anteriormente para hacer los ejercicios 5 y 6!
console.log(personsCars);
